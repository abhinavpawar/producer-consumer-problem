# README #

**Producer Consumer Problem**


*  One thread will produce the items and the other will consume. 
* Items are integers which will be added in a List whose max size can be 20.
* The thread which runs slow will wait and the other will notify the waiting thread when required.
* This simulation will continue till the user presses return key.
