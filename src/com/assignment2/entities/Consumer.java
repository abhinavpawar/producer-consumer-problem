package com.assignment2.entities;

import java.util.Vector;

public class Consumer implements Runnable {
	private final Vector sharedQueue;
	private final int SIZE;
	boolean consFlag = true;

	public Consumer(Vector sharedQueue, int size) {
		this.sharedQueue = sharedQueue;
		this.SIZE = size;
	}

	@Override
	public void run() {
		while (consFlag || sharedQueue.isEmpty() != true) {
			try {
				Thread.sleep(130);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			try {
				consume();
			} catch (InterruptedException ex) {
			}
		}

	}

	public void consume() throws InterruptedException {
		while (sharedQueue.isEmpty() == true) {
			synchronized (sharedQueue) {
				System.out.println("Queue is Empty. Consumer is waiting"
						+ "Size is:" + sharedQueue.size());
				sharedQueue.wait();
			}
		}
		// System.out.println("test");
		synchronized (sharedQueue) {
			sharedQueue.notifyAll();
			System.out.println("Consumed -" + sharedQueue.remove(0));
		}

	}

	public void consStop() {
		this.consFlag = false;

	}

}
