package com.assignment2.entities;

import java.util.Random;
import java.util.Vector;

public class Producer implements Runnable {
	private final Vector sharedQueue;
	private final int SIZE;
	boolean prodFlag = true;

	public Producer(Vector sharedQueue, int size) {
		this.sharedQueue = sharedQueue;
		this.SIZE = size;
	}

	@Override
	public void run() {
		int i = 0;
		while (prodFlag) {
			try {
				Thread.sleep(90);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (SIZE == sharedQueue.size()) {

				synchronized (sharedQueue) {
					System.out.println("Queue is Full Producer is waiting"
							+ sharedQueue.size());
					try {
						sharedQueue.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			} else {
				produce(i);
				i++;
			}
		}
	}

	public void produce(int i) {

		// Random random = new Random();
		int prod = i;
		// random.nextInt(100);
		synchronized (sharedQueue) {
			sharedQueue.add(prod);
			System.out.println("Produced -" + prod);
			sharedQueue.notifyAll();
		}

	}

	public void prodStop() {
		this.prodFlag = false;

	}

}
