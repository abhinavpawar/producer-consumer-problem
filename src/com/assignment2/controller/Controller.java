package com.assignment2.controller;

import com.assignment2.entities.*;

import java.io.*;

import java.util.*;

public class Controller {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		Vector sharedQueue = new Vector();
		int size = 20;
		//
		Producer producer = new Producer(sharedQueue, size);
		Consumer consumer = new Consumer(sharedQueue, size);
		Thread prodThread = new Thread(producer, "Producer");
		Thread consThread = new Thread(consumer, "Consumer");

		prodThread.start();
		consThread.start();
		scanner.nextLine();
		producer.prodStop();
		consumer.consStop();

	}
}
